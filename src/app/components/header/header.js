import React from "react";
import ReactLoading from 'react-loading';
import { connect } from "react-redux";
import classNames from "classnames";
import Link from "../../../../core/src/components/link";
import {bindActionCreators} from "redux";
import * as articlesAction from "../../actions/articlesAction";
import * as styles from "./header.css";
import { Navbar, Nav, NavItem, NavDropdown, MenuItem  } from "react-bootstrap";

const Header = (props) => {
  return (
    <div>
      <header id="header">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-sm-12">
            <div className={`${styles["header_top"]}`}>
              <div className={`${styles["header_top_left"]}`}>
                <ul className={`${styles["top_nav"]}`}>
                  <li>
                    <Link to='/'>HOME</Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-lg-10 col-md-10 col-sm-10">
            <div className={`${styles["header_bottom"]}`}>
              <h3> News </h3>
            </div>
          </div>
          <div className="col-lg-2 col-md-2 col-sm-2">
            <div className={`${styles["header_bottom"]}`}>
              {props.ajaxStatus && <ReactLoading type="spin" color="#444" />}
            </div>
          </div>
        </div>
      </header>
    </div>
  );
};

function mapStateToProps(state, ownProps) {
  return {
    ajaxStatus: state.ajaxStatus > 0
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(articlesAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);