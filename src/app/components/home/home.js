import React, { Component } from "react";
import ArticleListDiv from "../article/articleListDiv";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as articlesAction from "../../actions/articlesAction";


class Home extends Component {

  constructor(props, context) {
    super(props, context);
  }

  componentWillMount() {
    if(this.props.articleCategories) {
      this.props.actions.loadArticles();
    }
  }

  render() {
    if (this.props.articleCategories) {
      const articleCategories = Object.entries(this.props.articleCategories);
      return (
          <div className="container">
            <div className="row">
              {articleCategories.map(articleCategory =>
                      <ArticleListDiv key={articleCategory[0]}
                                      categoryId={articleCategory[0]}
                                      articleCategoryTitle={articleCategory[0]}
                                      articles={articleCategory[1]}/>
              )}
            </div>
          </div>
      );
    } else {
      return (
          <div className="container">
            <div className="row">
            </div>
          </div>
      );
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    articleCategories: state.articleCategories
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(articlesAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);