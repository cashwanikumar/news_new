import React, { Component } from "react";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as articlesAction from "../../actions/articlesAction";
import * as styles from "./article.css";

class EachArticle extends Component {

  componentWillMount() {
    {this.props.article && this.props.actions.loadBySpecificArticle("pid", this.props.match.params.id);}
  }

  render() {
    if (this.props.article && this.props.article[0]) {
      const article = this.props.article[0];
      return (
        <div className="container">
          <h2 className={`${styles["article-title"]}`}>{article.Title}</h2>
          <div className={`${styles["author-summary"]}`}>
            <span>By: <b>{article.Author}</b> ({article.Author_Email}) - {article.Date_Created}</span>
          </div>
          <br />
          <p className={`${styles["article-content"]}`} dangerouslySetInnerHTML={{ __html: article.Content }} />
          <br />
          <p>{article.URL}</p>
        </div>
      );
    }
    else {
      return (
        <div className="container"></div>
      );
    }
  }
}


function mapStateToProps(state, ownProps) {
  return {
    article: state.article
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(articlesAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EachArticle);