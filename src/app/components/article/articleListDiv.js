import React, { Component } from "react";
import ArticleLists from "./articleLists";
import * as styles from "./article.css";
import Link from "../../../../core/src/components/link";

export default class ArticleListDiv extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    if (this.props.categoryId) {
      return (
        <div className="col-lg-4 col-md-4 col-sm-4">
          <div className={`${styles["latest_post"]}`}>
            <h2><span><Link to={'/articles/' + this.props.articleCategoryTitle}>{this.props.articleCategoryTitle}</Link></span></h2>
            <div className={`${styles["latest_post_container"]}`}>
              <div id="prev-button">
                <i className="fa fa-chevron-up"></i>
              </div>
              <ul className={`${styles["latest_postnav"]}`}>
                {this.props.articles.map(article =>
                  <ArticleLists key={article.Post_ID} article={article}/>
                )}
              </ul>
              <div id="next-button">
                <i className="fa  fa-chevron-down"></i>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div></div>
      );
    }
  }
}