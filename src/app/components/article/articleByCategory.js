import React, { Component } from "react";
import _ from "lodash";
import ArticleLists from "./articleLists";
import * as styles from "./article.css";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as articlesAction from "../../actions/articlesAction";

class ArticleByCategory extends Component {

  componentWillMount() {
    {this.props.articlesOfCategory && this.props.actions.loadArticlesByCategory('ca', this.props.match.params.category)};
  }
  render() {
    if(this.props.articlesOfCategory) {
      return (
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12">
              <div className={`${styles["latest_post"]}`}>
                <h2><span>{this.props.match.params.category}</span></h2>
                <div className={`${styles["latest_post_container"]}`}>
                  <ul className={`${styles["latest_postnav"]}`}>
                    {_.values(this.props.articlesOfCategory).map(article =>
                            <ArticleLists key={article.Post_ID}
                                          article={article}/>
                    )}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } return (
        <div></div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    articlesOfCategory: state.articlesOfCategory
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(articlesAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleByCategory);