import React, { Component } from "react";
import * as styles from "./article.css";
import Link from "../../../../core/src/components/link";

export default class ArticleLists extends Component {
  render() {
    var marginStyle = {
      marginTop: "0px"
    };
    
    return (
      <li style={marginStyle}>
        <div className="media">
          <Link className={`${styles["media-left"]}`}  to={"/article/" + this.props.article.Post_ID}>
            <img alt="Loading article Image..." src={`https:\\\\${this.props.article.Featured_Image}`} />
          </Link>
          <div className={`${styles["media-body"]}`}>
            <Link className={`${styles["media-left"]}`} to={"/article/" + this.props.article.Post_ID}>
              {this.props.article.Title}
            </Link>
          </div>
        </div>
      </li>
    );
  }
}