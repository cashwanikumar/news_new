import _ from "lodash";
import initialState from './initialState';
import * as types from '../actions/actionTypes';

export const articleCategories = (state = initialState.articleCategories, action ='') => {
  switch (action.type) {
    case types.LOAD_ARTICLE_CATEGORIES_SUCCESS:
      return _.assign({}, state, _.mapValues(_.groupBy(action.articles, 'Category')));
    default:
      return state;
  }
};

export const article = (state = initialState.article, action ='') => {
  switch (action.type) {
    case types.LOAD_ARTICLE_SUCCESS:
      return _.assign({}, state, action.article);
    default:
      return state;
  }
};

export const articlesOfCategory = (state = initialState.articlesOfCategory, action ='') => {
  switch (action.type) {
    case types.LOAD_ARTICLES_BY_CATEGORY_SUCCESS:
      return _.assign({}, state, action.articles);
    default:
      return state;
  }
};

function actionTypeEndsInSuccess(type) {
  return type.substring(type.length - 8) == '_SUCCESS';
}

export const ajaxStatus = (state = initialState.ajaxStatus, action = '') => {
  if (action.type == types.BEGIN_AJAX_CALL) {
    return state + 1;
  } else if (action.type == types.AJAX_CALL_ERROR ||
      actionTypeEndsInSuccess(action.type)) {
    return state - 1;
  }
  return state;
};