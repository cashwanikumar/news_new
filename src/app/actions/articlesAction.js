import * as types from "./actionTypes";
import {beginAjaxCall, ajaxCallError} from "./ajaxLoader";
import Axios from "axios";
const delay = 0;

const url = "https://cors-anywhere.herokuapp.com/http://139.59.106.116";
const ajaxConfig = {
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  }
};

export function loadArticlesSuccess(articles) {
  return {
    type: types.LOAD_ARTICLE_CATEGORIES_SUCCESS, articles
  };
}

export function loadArticleSuccess(article) {
  return {
    type: types.LOAD_ARTICLE_SUCCESS, article
  };
}
export function loadArticlesByCategorySuccess(articles) {
  return {
    type: types.LOAD_ARTICLES_BY_CATEGORY_SUCCESS, articles
  };
}

export function loadArticles() {
  return function(dispatch) {
    dispatch(beginAjaxCall());
    setTimeout(() => {
      Axios.get(url + "/articles/", ajaxConfig)
        .then(response => {
          dispatch(loadArticlesSuccess(response.data));
        }).catch(error => {
          dispatch(ajaxCallError);
          console.log(error);
        });
    }, delay);
  };
}

export function loadBySpecificArticle(searchItem, articleId) {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    setTimeout(() => {
      Axios.get(`${url}/articles/?${searchItem}=${articleId}`, ajaxConfig)
        .then(response => {
          dispatch(loadArticleSuccess(response.data));
        }).catch(error => {
          dispatch(ajaxCallError);
          console.log(error);
        });
    }, delay);
  };
}

export function loadArticlesByCategory(searchItem, articleCategory) {
  return function (dispatch) {
    dispatch(beginAjaxCall());
    setTimeout(() => {
      Axios.get(`${url}/articles/?${searchItem}=${articleCategory}`)
        .then(response => {
          dispatch(loadArticlesByCategorySuccess(response.data));
        }).catch(error => {
          dispatch(ajaxCallError);
          console.log(error);
        });
    }, delay);
  };
}