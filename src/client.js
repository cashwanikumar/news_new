import {trackPageView} from "../core/src/utils/analytics";

import thunk from "redux-thunk";
import initialState from "./app/reducers/initialState";
import * as articlesReducers from "./app/reducers/articlesReducer";

export const reduxInitialState = initialState;
export const reduxReducers = articlesReducers;
export const reduxMiddleware = [thunk];
export const onPageChange = function() {
  trackPageView().catch();
};

if (module.hot) module.hot.accept();